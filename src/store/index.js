import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list: []
  },
  mutations: {
    SET_LIST(state, data) {
      state.list = data
    }
  },
  actions: {
    async loadProducts({commit}){
      let response = await axios.get('https://run.mocky.io/v3/b7d36eea-0b3f-414a-ba44-711b5f5e528e').catch(error => console.log(error))
      if(!response) return false
      commit('SET_LIST', response.data)
    }
  },
  modules: {
  }
})

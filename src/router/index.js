import Vue from 'vue'
import VueRouter from 'vue-router'
import CardsWrap from '../views/CardsWrap.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'CardsWrap',
    component: CardsWrap
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
